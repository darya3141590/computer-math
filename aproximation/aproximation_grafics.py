import matrix.matrix as mx
import matplotlib.pyplot as plt
import aproximation.aproximation as apr

def least_square_method_with_one_unknown(A, b):
    x_array = []
    y_array = []
    for x in range(0*10, 5*10, 1):
        x_array.append(x/10)
        y_array.append(((A[0][0]*(x/10)-b[0][0])**2)+((A[1][0]*(x/10)-b[1][0])**2))
    point_x = apr.least_square_method(A, b)
    point_y = (A[0][0]*point_x-b[0][0])**2+(A[1][0]*point_x-b[1][0])**2
    plt.scatter(point_x, point_y, color="orange") 
    plt.plot(x_array, y_array)
    plt.title('МНК с одной неизвестной')
    plt.grid(True)
    plt.show()

def linear_aproximation(data_xy, x_array):
    y_array = apr.linear_aproximation(data_xy, x_array)[0]
    data_x_array = mx.get_col(data_xy, 0)
    data_y_array = mx.get_col(data_xy, 1)
    plt.scatter(x_array, y_array, color="orange")
    plt.scatter(data_x_array, data_y_array)
    plt.plot(x_array, y_array)
    plt.title('Линейная аппроксимация')
    plt.grid(True)
    plt.show()

def second_degree_polinomial(x, point_x, data_xy):
    x_array = []
    y_array = []
    data_x_array = mx.get_col(data_xy, 0)
    data_y_array = mx.get_col(data_xy, 1)
    point_y= apr.second_degree_polinomial(x, point_x)[0]
    for i in range(-5*10, 6*10, 1):
        x_array.append(i/10)
        y_array.append(x[0][0]*(i/10)**2+x[1][0]*(i/10)+x[2][0])
    plt.scatter(point_x, point_y, color="orange")
    plt.scatter(data_x_array, data_y_array)
    plt.plot(x_array, y_array)
    plt.title('Аппроксимация полиномом 2-й степени')
    plt.grid(True)
    plt.show()

def third_degree_polinomial(x, point_x, data_xy):
    x_array = []
    y_array = []
    data_x_array = mx.get_col(data_xy, 0)
    data_y_array = mx.get_col(data_xy, 1)
    point_y= apr.third_degree_polinomial(x, point_x)[0]
    for i in range(0*10, 6*10, 1):
        x_array.append(i/10)
        y_array.append(x[0][0]*(i/10)**3+x[1][0]*(i/10)**2+x[2][0]*(i/10)+x[3][0])
    plt.scatter(data_x_array, data_y_array)
    plt.plot(x_array, y_array)
    plt.scatter(point_x, point_y, color="orange")
    plt.title('Аппроксимация полиномом 3-й степени')
    plt.grid(True)
    plt.show()


    
def apromaxiation_show():
    A = [[2], [3]]
    b = [[4], [9]]
    least_square_method_with_one_unknown(A, b)
    data_xy = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
    x_array = [[1], [3], [5]]
    linear_aproximation(data_xy, x_array)
    x = [[0.13], [0.07], [1.89]]
    x_array = [[1], [3], [5]]
    second_degree_polinomial(x, x_array, data_xy)
    x = [[0.48], [-4.80], [13.96], [-7.64]]
    x_array = [[1], [3], [5]]
    third_degree_polinomial(x, x_array, data_xy)
