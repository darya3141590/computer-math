import SLE.SLE as sle
import matrix.matrix as mx

def least_square_method(A, b):
    transposed_matrix = mx.matrix_transposition(A)
    first_matrix = mx.matrix_multiplication(transposed_matrix, A)
    sec_matrix = mx.matrix_multiplication(transposed_matrix, b)
    return sle.sle(first_matrix, sec_matrix)

def linear_aproximation(data_xy, x_array):
    A = mx.copy2d(data_xy)
    x_array = mx.copy2d(x_array)
    for i in range(len(A)):
        A[i][-1] = 1
    transposed_matrix = mx.matrix_transposition(A)
    b = [[i[-1]] for i in data_xy]
    first_matrix = mx.matrix_multiplication(transposed_matrix, A)
    sec_matrix = mx.matrix_multiplication(transposed_matrix, b)
    search_x = sle.sle(first_matrix, sec_matrix)
    x = [[search_x[0][-1]], [search_x[1][-1]]]
    for i in x_array:
        i.append(1)
    y_array = mx.matrix_transposition(mx.matrix_multiplication(x_array, x))
    return y_array

def second_degree_polinomial(x, x_array):
    x_array = mx.copy2d(x_array)
    for i in x_array:
        i.insert(0, i[0]**2)
        i.append(1)
    y_array = mx.matrix_transposition(mx.matrix_multiplication(x_array, x))
    return y_array

def third_degree_polinomial(x, x_array):
    x_array = mx.copy2d(x_array)
    for i in x_array:
        i.insert(0, i[0]**2)
        i.insert(0, i[1]**3)
        i.append(1)
    y_array = mx.matrix_transposition(mx.matrix_multiplication(x_array, x))
    return y_array

