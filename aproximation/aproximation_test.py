import aproximation.aproximation as apr

def test_least_square_method():
     A = [[2], [3]]
     b = [[4], [9]]
     exp = 2.6923076923076925
     res = apr.least_square_method(A, b)
     assert res == exp

def test_linear_aproximation():
     data_xy = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
     x_array = [[1], [3], [5]]
     exp = [[1.660098522167488, 3.6305418719211824, 5.600985221674877]]
     res = apr.linear_aproximation(data_xy, x_array)
     assert res == exp

def test_second_degree_polinomial():
     x = [[0.13], [0.07], [1.89]]
     x_array = [[1], [3], [5]]
     exp = [[2.09, 3.2699999999999996, 5.49]]
     res = apr.second_degree_polinomial(x, x_array)
     assert res == exp

def test_third_degree_polinomial():
     x = [[0.48], [-4.80], [13.96], [-7.64]]
     x_array = [[1], [3], [5]]
     exp = [[2.000000000000001, 4.000000000000008, 2.1600000000000117]]
     res = apr.third_degree_polinomial(x, x_array)
     assert res == exp