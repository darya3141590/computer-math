import SLE.SLE as sle


def test_sle():
     a = [[-1, 2, 6],
          [3, -6, 0],
          [1, 0, 6]]
     b = [[15],
          [-9],
          [5]]
     exp = [[1.0, 0.0, 0.0, -7.0],
           [0.0, 1.0, 0.0, -2.0],
           [0.0, 0.0, 1.0, 2.0]]
     res = sle.sle(a, b)
     assert res == exp


