import matrix.matrix as mx
import vector.vectora as vec

def check(elem):
    return elem != 0

def multiplication_and_substraction(a, stroke, which_stroke, end, step):
    for i in range(which_stroke, end, step): #откуда считать, до конца
        mult = vec.multiplication(mx.get_row(a, stroke), mx.get_row(a, i)[stroke])
        a[i] = vec.difference(mx.get_row(a, i), mult)
    return a

def run(a, start, end, step):
    for i in range(start, end, step): #для деления
        if not check(mx.get_row(a, i)[i]):
            for s in range(i, end):
                if mx.get_row(a, s)[i] != 0:
                    a[i], a[s] = a[s], a[i]
                    break
        a[i] = vec.division(mx.get_row(a, i), mx.get_row(a, i)[i])
        if start != 0:
            l = end+1
            if i!=l:
                a = multiplication_and_substraction(a, i, i-1, end, step)
        else:
            l = end-1
            if i!=l:
                a = multiplication_and_substraction(a, i, i+1, end, step) #нулевую и следущую, откуда считать
    return a

def sle(a, b, do_copy = True):
    a = mx.copy2d(a, do_copy)
    if len(a) == 1:
        return b[0][0]/a[0][0]
    else:
        for i in range(len(b)):
            for c in range(len(b[0])):
                a[i].append(mx.get_row(b, i)[c])
        a = [[float(a[i][j]) for j in range(len(a[0]))] for i in range(len(a))]
        direct_run = run(a, 0, len(a), 1)
        reverse_run= run(direct_run, len(a)-1, -1, -1)
        return reverse_run

