import Macloren.macloren as mac

def test_search_factorial():
     n = 4
     exp = 24
     res = mac.search_factorial(n)
     assert res == exp

def test_search_exponent():
     n = 1
     x = 5
     exp = 6
     res = mac.search_exponent(n, x)
     assert res == exp

def test_search_sin():
     n = 1
     x = 2
     exp = 0.6666666666666667
     res = mac.search_sin(n, x)
     assert res == exp

def test_search_cos():
     n = 1
     x = 2
     exp = -1
     res = mac.search_cos(n, x)
     assert res == exp

def test_search_arccos():
     n = 2
     x = 2
     exp = -4.163333333333332
     res = mac.search_arccos(n, x)
     assert res == exp

def test_search_arcsin():
     n = 1
     x = 3
     exp = 7.5
     res = mac.search_arcsin(n, x)
     assert res == exp
