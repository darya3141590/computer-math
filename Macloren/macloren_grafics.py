import matrix.matrix as mx
import matplotlib.pyplot as plt
import Macloren.macloren as mac

def exponent(n, interval):
    e = 2.7
    x_array = []
    y_array = []
    exp_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array.append(x/10)
        y_array.append(mac.search_exponent(n, x/10))
        exp_y_array.append(e**(x/10))
    plt.plot(x_array, exp_y_array)
    plt.plot(x_array, y_array) 
    plt.title('Разложение в ряд Маклорена f(x)=e^x')
    plt.grid(True)
    plt.show()

def sin(n, interval):
    x_array = []
    y_array = []
    sin_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array.append(x/10)
        y_array.append(mac.search_sin(n, x/10))
        sin_y_array.append(mac.search_sin(n+10, x/10))
    plt.plot(x_array, sin_y_array)
    plt.plot(x_array, y_array) 
    plt.title('Разложение в ряд Маклорена f(x)=sinx')
    plt.grid(True)
    plt.show()

def cos(n, interval):
    x_array = []
    y_array = []
    cos_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array.append(x/10)
        y_array.append(mac.search_cos(n, x/10))
        cos_y_array.append(mac.search_cos(n+10, x/10))
    plt.plot(x_array, cos_y_array)
    plt.plot(x_array, y_array) 
    plt.title('Разложение в ряд Маклорена f(x)=cosx')
    plt.grid(True)
    plt.show()

def arccos(n, interval):
    x_array = []
    y_array = []
    arccos_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array.append(x/10)
        y_array.append(mac.search_arccos(n, x/10))
        arccos_y_array.append(mac.search_arccos(n+10, x/10))
    plt.plot(x_array, arccos_y_array)
    plt.plot(x_array, y_array) 
    plt.title('Разложение в ряд Маклорена f(x)=arccosx')
    plt.grid(True)
    plt.show()

def arcsin(n, interval):
    x_array = []
    y_array = []
    arcsin_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array.append(x/10)
        y_array.append(mac.search_arcsin(n, x/10))
        arcsin_y_array.append(mac.search_arcsin(n+10, x/10))
    plt.plot(x_array, arcsin_y_array)
    plt.plot(x_array, y_array) 
    plt.title('Разложение в ряд Маклорена f(x)=arcsinx')
    plt.grid(True)
    plt.show()




    
def macloren_show():
    n = 2
    interval = (-5, 2)
    exponent(n, interval)
    n = 1
    interval = (-4, 4)
    sin(n, interval)
    n = 5
    interval = (-6, 6)
    cos(n, interval)
    n = 0
    interval = (-1, 1)
    arccos(n, interval)
    n = 3
    interval = (-1, 1)
    arcsin(n, interval)
