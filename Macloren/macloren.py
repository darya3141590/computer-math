
def search_factorial(n):
    factorial = 1
    for i in range(1, n+1):
        factorial*=i
    if n==0:
            factorial = 1
    return factorial

def search_exponent(n, x):
    exponent = 0
    for i in range(0,n+1):
        exponent+=x**i/search_factorial(i)
    return exponent

def search_sin(n, x):
    sin = 0
    for i in range(0,n+1):
        sin+=(((-1)**i)*(x**(2*i+1)))/search_factorial(2*i+1)
    return sin

def search_cos(n, x):
    cos = 0
    for i in range(0,n+1):
        cos+=(((-1)**i)*(x**(2*i)))/search_factorial(2*i)
    return cos

def search_arccos(n, x):
    pi = 3.14
    arccos = 0
    for i in range(0,n+1):
        arccos+=search_factorial(2*i)*x**(2*i+1)/(4**i*(search_factorial(i))**2*(2*i+1))
    return pi/2 - arccos

def search_arcsin(n, x):
    pi = 3.14
    arcsin= 0
    for i in range(0,n+1):
        arcsin+=search_factorial(2*i)*x**(2*i+1)/(4**i*(search_factorial(i))**2*(2*i+1))
    return arcsin

