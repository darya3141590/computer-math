import vector.vectora as vec


def copy2d(a, do_copy=True):
    if do_copy:
        return [b[:] for b in a]

def comparison_of_subarray_length(a):
    for i, k in enumerate(a):
        if len(a[0]) != len(a[i]):
            return False
    return True


def compare_length(a, b):
    if not comparison_of_subarray_length(a):
        return False
    elif not comparison_of_subarray_length(b):
        return False
    else:
        return len(a) == len(b)


def plus(a):
    return a[0] + a[1]


def minus(a):
    return a[0] - a[1]


def matrix_addition(a, b):
    ensure_shape(a, b)
    sum_matrix = []
    for i in range(len(a)):
        sum = vec.summ(a[i], b[i])
        sum_matrix.append(sum)
    return sum_matrix


def matrix_substraction(a, b):
    sub_matrix = []
    for i in range(len(a)):
        sub = vec.difference(a[i], b[i])
        sub_matrix.append(sub)
    return sub_matrix


def ensure_shape(a, b):
    if not compare_length(a, b):
        raise ValueError('Длины разные')


def matrix_transposition(a):
    return [[a[j][i] for j in range(len(a))] for i in range(len(a[0]))]


def multiplication_of_a_matrix_by_a_scalar(a, k):
    mult_scal = []
    for i in a:
        mult = vec.multiplication(i, k)
        mult_scal.append(mult)
    return mult_scal


def matrix_multiplication(a, b):
    mult = [[0 for j in range(len(b[0]))] for i in range(len(a))]
    try:
        transpos_b = matrix_transposition(b)
        for i in range(len(a)):
            for j in range(len(transpos_b)):
                scal_mult = vec.scal_multiplication(a[i], transpos_b[j])
                mult[i][j] = scal_mult
    except IndexError:
        raise IndexError('Количество строк одной матрицы не равно количесвту столбцов другой')
    return mult


def get_row(a, i, do_copy=True):
    a = copy2d(a, do_copy)
    return a[i]


def get_col(a, i):
    new_a = matrix_transposition(a)
    return new_a[i]


def permuting_rows(a, row_index, where, do_copy=True):
    a = copy2d(a, do_copy)
    for j in range(len(a)):
        for i in range(len(a[0])):
            s = a[row_index][i]
            a[row_index][i] = a[where][i]
            a[where][i] = s
    return a


def multiplication_string_by_a_scalar(index, a, k, do_copy=True):
    a = copy2d(a, do_copy)
    s = get_row(a, index)
    a[index] = vec.multiplication(s, k)
    return a


def summa_multiplication_str_by_scalar(a, first_index, k, second_index, m, do_copy=True):
    a = copy2d(a, do_copy)
    first_s = get_row(a, first_index)
    second_s = get_row(a, second_index)
    first_s = vec.multiplication(first_s, k)
    second_s = vec.multiplication(second_s, m)
    summ = vec.summ(first_s, second_s)
    a[first_index] = summ
    return a


def substraction_multiplication_str_by_scalar(a, first_index, k, second_index, m, do_copy=True):
    a = copy2d(a, do_copy)
    first_s = get_row(a, first_index)
    second_s = get_row(a, second_index)
    first_s = vec.multiplication(first_s, k)
    second_s = vec.multiplication(second_s, m)
    diff = vec.difference(first_s, second_s)
    a[first_index] = diff
    return a
