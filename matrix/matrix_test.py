import matrix.matrix as mx


def test_summ_m():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    b = [[3, 4],
         [-1, 3],
         [8, 2]]
    exp = [[4, 6],
           [2, 7],
           [15, 10]]
    res = mx.matrix_addition(a, b)
    assert res == exp


def test_difference_m():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    b = [[3, 4],
         [-1, 3],
         [8, 2]]
    exp = [[-2, -2],
           [4, 1],
           [-1, 6]]
    res = mx.matrix_substraction(a, b)
    assert res == exp


def test_transposition():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    ca = mx.copy2d(a)
    exp = [[1, 3, 7],
           [2, 4, 8]]
    res = mx.matrix_transposition(ca)
    assert res == exp


def test_multiplic_scal():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    k = 4
    ca = mx.copy2d(a)
    exp = [[4, 8],
           [12, 16],
           [28, 32]]
    res = mx.multiplication_of_a_matrix_by_a_scalar(ca, k)
    assert res == exp


def test_multiplication_matrix():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    b = [[3, 4],
         [-1, 3]]
    exp = [[1, 10],
           [5, 24],
           [13, 52]]
    res = mx.matrix_multiplication(a, b)
    assert res == exp



def test_index_stroke():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    i = 2
    ca = mx.copy2d(a)
    exp = [7, 8]
    res = mx.get_row(ca, i)
    assert res == exp


def test_index_col():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    i = 1
    ca = mx.copy2d(a)
    exp = [2, 4, 8]
    res = mx.get_col(ca, i)
    assert res == exp


def test_permuting_rows():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    ca = mx.copy2d(a)
    row_index = 2
    where = 1
    exp = [[1, 2],
           [7, 8],
           [3, 4]]
    res = mx.permuting_rows(ca, row_index, where)
    assert res == exp


def test_multiplic_str_by_scal():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    ca = mx.copy2d(a)
    index = 1
    k = 4
    exp = [[1, 2],
           [12, 16],
           [7, 8]]
    res = mx.multiplication_string_by_a_scalar(index, ca, k)
    assert res == exp


def test_sum_multiplication_str_by_scalar():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    ca = mx.copy2d(a)
    k = 4
    first_index = 1
    m = 3
    second_index = 2
    exp = [[1, 2],
           [33, 40],
           [7, 8]]
    res = mx.summa_multiplication_str_by_scalar(ca, first_index, k, second_index, m)
    assert res == exp


def test_diff_multiplication_str_by_scalar():
    a = [[1, 2],
         [3, 4],
         [7, 8]]
    ca = mx.copy2d(a)
    k = 4
    first_index = 1
    m = 3
    second_index = 2
    exp = [[1, 2],
           [-9, -8],
           [7, 8]]
    res = mx.substraction_multiplication_str_by_scalar(ca, first_index, k, second_index, m)
    assert res == exp


def test_compare_matrix():
    a = [[1, 2],
         [3, 4],
         [7, 8],
         [10, 2]]
    b = [[3, 4],
         [-1, 3],
         [8, 2]]
    exp = False
    res = mx.compare_length(a, b)
    assert res == exp

def test_multiplication_string_on_column():
     A = [[2, 3]]
     b = [[4], [9]]
     exp = [[35]]
     res = mx.matrix_multiplication(A, b)
     assert res == exp
