import SLE.SLE as sle
import matrix.matrix as mx

def searching_a_and_b(data):
    A=[[data[0][0], 1], [data[1][0], 1]]
    b = [[i] for i in mx.get_col(data, -1)]
    a_and_b = mx.get_col(sle.sle(A, b), -1)
    return a_and_b

def searching_line_equation(model, x):
    a = model[0]
    b = model[1]
    y = a*x + b
    return y

def division_into_subarrays(data):
    datas = []
    model = []
    x_array = []
    for i in range(len(data)-1):
        datas.append([])
        datas[i].append(data[i])
        datas[i].append(data[i+1])
    for i in datas:
        model.append(searching_a_and_b(i))
        x_array.append(sorted(mx.get_col(i, 0)))
    return model, x_array

def searching_line_equation_second(model, x_array, x):
    for i in x_array:
        if (i[0]<=x) and (x<=i[1]):
            a = model[x_array.index(i)][0]
            b = model[x_array.index(i)][1]
            y = a*x + b
            break
        else:
            continue
    return y

def polinom_Lagrange(length, i, polinom, x, x_array, y_array):
    for j in range(length):
        if j!=i:
            polinom*=(x-x_array[j])/(x_array[i]-x_array[j])
    return y_array[i]*polinom

def searching_L(x_array, data, x):
    new_x_array = sorted(list(set(el for i in x_array for el in i)))
    y_array = mx.get_col(data, -1)
    polinom=1
    L=0
    for i in range(len(data)):
        L+=polinom_Lagrange(len(data), i, polinom, x, new_x_array, y_array)
    return L



