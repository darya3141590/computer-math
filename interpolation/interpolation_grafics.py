import matplotlib.pyplot as plt
import interpolation.interpolation as interp
import matrix.matrix as mx

def linear_interpolation_and_extrapolation(two_points, point_x):
    x = mx.get_col(two_points, 0)
    y = mx.get_col(two_points, 1)
    # x = [two_points[0][0], two_points[1][0]]
    # y = [two_points[0][1], two_points[1][1]]
    point_y = interp.searching_line_equation(interp.searching_a_and_b(two_points), point_x)
    x.append(point_x)
    y.append(point_y)
    plt.scatter(x, y)   
    plt.scatter(point_x, point_y, color="orange") 
    plt.plot(x, y)
    plt.title('Линейная интерполяция и экстраполяция')
    plt.grid(True)
    plt.show()

def piecewise_linear_interpolation_and_extrapolation(data, point_x):
    x = mx.get_col(data, 0)
    y = mx.get_col(data, 1)
    # x = [data[0][0], data[1][0], data[2][0], data[3][0]]
    # y = [data[0][1], data[1][1], data[2][1], data[3][1]]
    point_y = interp.searching_line_equation_second(interp.division_into_subarrays(data)[0], interp.division_into_subarrays(data)[1], point_x)
    if point_x<x[0]:
        x.insert(0, point_x)
        y.insert(0, point_y)
    elif point_x>x[-1]:
        x.append(point_x)
        y.append(point_y)
    plt.scatter(x, y)  
    plt.scatter(point_x, point_y, color="orange") 
    plt.plot(x, y)
    plt.title('Кусочно-линейная интерполяция и экстраполяция')
    plt.grid(True)
    plt.show()

def polinom(data, interval):
    x_array = interp.division_into_subarrays(data)[1]
    y_array = []
    x_array_for_y_array = []
    for x in range(interval[0]*10, interval[1]*10, 1):
        x_array_for_y_array.append(x/10)
        y_array.append(interp.searching_L(x_array, data, x/10))
    # plt.scatter(x_array_for_y_array, y_array)
    plt.plot(x_array_for_y_array, y_array)
    plt.title('Полином Лагранжа')
    plt.grid(True)
    plt.show()

    
def interpolation_show():
    two_points = [[2, 5], [6, 9]]
    x = 4
    linear_interpolation_and_extrapolation(two_points, x)
    data = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
    x = 2
    piecewise_linear_interpolation_and_extrapolation(data, x)
    interval = (0, 7)
    polinom(data, interval)
