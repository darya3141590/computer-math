import interpolation.interpolation as interp
import matrix.matrix as mx

def test_search_a_b():
     two_points = [[2, 5], [6, 9]]
     exp = [1, 3]
     res = interp.searching_a_and_b(two_points)
     assert res == exp

def test_search_line_eq():
     two_points = [[2, 5], [6, 9]]
     model = interp.searching_a_and_b(two_points)
     x = 4
     exp = 7.0
     res = interp.searching_line_equation(model, x)
     assert res == exp

def test_division_into_subarrays():
     data = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
     exp = ([[1.0, 1.0], [-2.0000000000000018, 10.000000000000005], [1.6, -2.600000000000001]], [[1, 3], [3, 3.5], [3.5, 6]])
     res = interp.division_into_subarrays(data)
     assert res == exp

def test_search_line_eq_2():
     data = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
     model = interp.division_into_subarrays(data)[0]
     x_array = interp.division_into_subarrays(data)[1]
     x = 2
     exp = 3
     res = interp.searching_line_equation_second(model, x_array, x)
     assert res == exp

def test_polinom():
     data = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
     length = len(data)
     i = 0
     polinom = 1
     new_x_array = [1, 3, 3.5, 6]
     y_array = mx.get_col(data, -1)
     x = 1
     exp = 2
     res = interp.polinom_Lagrange(length, i, polinom, x, new_x_array, y_array)
     assert res == exp

def test_search_L():
     data = [[1, 2], [3, 4], [3.5, 3], [6, 7]]
     x_array = interp.division_into_subarrays(data)[1]
     x = 1
     exp = 2
     res = interp.searching_L(x_array, data, x)
     assert res == exp