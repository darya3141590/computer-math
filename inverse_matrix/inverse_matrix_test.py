import inverse_matrix.inverse_matrix as inv

def test_iverse_matrix():
     a = [[1, 2],
          [3, 4]]
     exp = [[-2.0, 1.0],
           [1.5, -0.5]]
     res = inv.searching_inverse_matrix(a)
     assert res == exp