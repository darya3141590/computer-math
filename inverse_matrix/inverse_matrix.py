import SLE.SLE as sle
import matrix.matrix as mx

def searching_identity_matrix(a):
    id_m = [[0 for j in range(len(a[0]))] for i in range(len(a))]
    for i in range(len(a)):
        id_m[i][i] = 1
    return id_m

def searching_inverse_matrix(a, do_copy = True):
    a = mx.copy2d(a, do_copy)
    id_m = searching_identity_matrix(a)
    inv_m = sle.sle(a, id_m)
    for i in range(len(inv_m)):
        del inv_m[i][0:2]
    return inv_m
